/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 13:30:01 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:37:53 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

double	multi_scalar(t_vec *d1, t_vec *d2)
{
	return ((double)(d1->x * d2->x + d1->y * d2->y + d1->z * d2->z));
}

t_vec	vec_minus(t_vec *d1, t_vec *d2)
{
	t_vec a;

	a.x = d1->x - d2->x;
	a.y = d1->y - d2->y;
	a.z = d1->z - d2->z;
	return (a);
}

t_vec	vec_plus(t_vec *d1, t_vec *d2)
{
	t_vec a;

	a.x = d1->x + d2->x;
	a.y = d1->y + d2->y;
	a.z = d1->z + d2->z;
	return (a);
}

t_vec	vec_multi_t(t_vec *d1, double t)
{
	t_vec a;

	a.x = d1->x * t;
	a.y = d1->y * t;
	a.z = d1->z * t;
	return (a);
}

t_vec	vec_invert(t_vec *d1)
{
	t_vec a;

	a.x = d1->x * -1;
	a.y = d1->y * -1;
	a.z = d1->z * -1;
	return (a);
}
