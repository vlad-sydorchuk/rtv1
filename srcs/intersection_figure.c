/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersection_figure.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 07:41:04 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:23:45 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

int		intersection_sphere(t_rtv *rtv, t_vec *org, t_vec *dir, int i)
{
	t_disc	disc;
	t_vec	w_x;

	w_x = vec_minus(org, rtv->sp[i]->crd);
	disc.a = multi_scalar(dir, dir);
	disc.b = 2 * multi_scalar(dir, &w_x);
	disc.c = multi_scalar(&w_x, &w_x) - pow(rtv->sp[i]->r, 2);
	disc.d = pow(disc.b, 2) - 4 * disc.a * disc.c;
	if (disc.d < 0)
		return (0);
	else
	{
		rtv->t1 = (-disc.b + sqrt(disc.d)) / (2 * disc.a);
		rtv->t2 = (-disc.b - sqrt(disc.d)) / (2 * disc.a);
		rtv->t = (rtv->t1 <= rtv->t2) ? rtv->t1 : rtv->t2;
	}
	return (1);
}

int		intersection_cilinder(t_rtv *rtv, t_vec *org, t_vec *dir, int i)
{
	t_disc	disc;
	t_vec	w_x;
	t_vec	d_n;

	d_n = normalization(rtv->clndr[i]->dir);
	w_x = vec_minus(org, rtv->clndr[i]->crd);
	disc.a = multi_scalar(dir, dir) - pow(multi_scalar(dir, &d_n), 2);
	disc.b = 2 * (multi_scalar(dir, &w_x) -
		(multi_scalar(dir, &d_n) * multi_scalar(&w_x, &d_n)));
	disc.c = multi_scalar(&w_x, &w_x) -
		pow(multi_scalar(&w_x, &d_n), 2) - pow(rtv->clndr[i]->r, 2);
	disc.d = pow(disc.b, 2) - 4 * disc.a * disc.c;
	if (disc.d < 0)
		return (0);
	else
	{
		rtv->t1 = (-disc.b + sqrt(disc.d)) / (2 * disc.a);
		rtv->t2 = (-disc.b - sqrt(disc.d)) / (2 * disc.a);
		rtv->t = (rtv->t1 <= rtv->t2) ? rtv->t1 : rtv->t2;
	}
	return (1);
}

int		intersection_cone(t_rtv *rtv, t_vec *org, t_vec *dir, int i)
{
	t_disc	disc;
	double	k;
	t_vec	w_x;
	t_vec	d_n;

	d_n = normalization(rtv->cone[i]->dir);
	k = 1 + pow(rtv->cone[i]->k, 2);
	w_x = vec_minus(org, rtv->cone[i]->crd);
	disc.a = multi_scalar(dir, dir) - k * pow(multi_scalar(dir, &d_n), 2);
	disc.b = 2 * (multi_scalar(dir, &w_x) - k *
		multi_scalar(dir, &d_n) * multi_scalar(&w_x, &d_n));
	disc.c = multi_scalar(&w_x, &w_x) - k * pow(multi_scalar(&w_x, &d_n), 2);
	disc.d = pow(disc.b, 2) - 4 * disc.a * disc.c;
	if (disc.d < 0)
		return (0);
	else
	{
		rtv->t1 = (-disc.b + sqrt(disc.d)) / (2 * disc.a);
		rtv->t2 = (-disc.b - sqrt(disc.d)) / (2 * disc.a);
		rtv->t = (rtv->t1 <= rtv->t2) ? rtv->t1 : rtv->t2;
	}
	return (1);
}

int		intersection_ftr(t_rtv *rtv, t_vec *org, t_vec *dir, int i)
{
	double	a;
	double	b;
	t_vec	n;
	t_vec	s;

	n = normalization(rtv->ftr[i]->dir);
	s = vec_minus(rtv->ftr[i]->crd, org);
	a = multi_scalar(&s, &n);
	b = multi_scalar(dir, &n);
	if (b == 0)
		return (0);
	if (a / b < 0)
		return (0);
	rtv->t = a / b;
	return (1);
}

void	detect(t_rtv *rtv)
{
	int		i;

	i = -1;
	while (++i < rtv->c_sp)
		if (intersection_sphere(rtv, rtv->org, rtv->dir, i))
			set_flag(rtv, 1);
	i = -1;
	while (++i < rtv->c_clndr)
		if (intersection_cilinder(rtv, rtv->org, rtv->dir, i))
			set_flag(rtv, 2);
	i = -1;
	while (++i < rtv->c_cone)
		if (intersection_cone(rtv, rtv->org, rtv->dir, i))
			set_flag(rtv, 3);
	i = -1;
	while (++i < rtv->c_ftr)
		if (intersection_ftr(rtv, rtv->org, rtv->dir, i))
			set_flag(rtv, 4);
}
