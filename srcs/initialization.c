/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialization.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 13:33:16 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 07:36:43 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void	init_zero(t_rtv *rtv)
{
	rtv->t = 0;
	rtv->t1 = 0;
	rtv->t2 = 0;
	rtv->t_m = 0;
	rtv->fong = 0;
	rtv->angle = 0;
	rtv->clr = 0;
	rtv->c_sp = 0;
	rtv->c_clndr = 0;
	rtv->c_cone = 0;
	rtv->c_ftr = 0;
}
