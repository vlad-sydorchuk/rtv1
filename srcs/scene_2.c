/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 04:50:12 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:33:12 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

static void	init_ftr(t_rtv *rtv)
{
	create_vec(&(rtv->ftr[0]->crd), 0, -400, -600);
	create_vec(&(rtv->ftr[0]->dir), 0, 400, -70);
}

static void	init_clndr(t_rtv *rtv)
{
	create_vec(&(rtv->clndr[0]->crd), 0, 0, -600);
	create_vec(&(rtv->clndr[0]->dir), 1, 2, 0);
	rtv->clndr[0]->r = 70;
}

void		scene_2(t_rtv *rtv)
{
	rtv->t = 0;
	rtv->angle = 0;
	rtv->fong = 0;
	rtv->c_sp = 0;
	rtv->c_clndr = 1;
	rtv->c_cone = 0;
	rtv->c_ftr = 1;
	rtv->clndr = create_cilinder(1);
	rtv->ftr = create_ftr(1);
	init_clndr(rtv);
	init_ftr(rtv);
	create_vec(&(rtv->org), 0, 0, 900);
	create_vec(&(rtv->dir), 0, 0, -1);
	create_vec(&(rtv->lgh), 0, 0, 0);
}
