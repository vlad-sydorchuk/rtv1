/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene_8.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 06:18:46 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:37:15 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

static void	init_sphere(t_rtv *rtv)
{
	create_vec(&(rtv->sp[0]->crd), 0, 0, -550);
	rtv->sp[0]->r = 130;
	create_vec(&(rtv->sp[1]->crd), 200, 200, -590);
	rtv->sp[1]->r = 60;
	create_vec(&(rtv->sp[2]->crd), 50, 300, -590);
	rtv->sp[2]->r = 60;
	create_vec(&(rtv->sp[3]->crd), 300, 450, -390);
	rtv->sp[3]->r = 60;
	create_vec(&(rtv->sp[4]->crd), -200, -200, -590);
	rtv->sp[4]->r = 60;
	create_vec(&(rtv->sp[5]->crd), 300, -300, -590);
	rtv->sp[5]->r = 60;
	create_vec(&(rtv->sp[6]->crd), -400, -400, -590);
	rtv->sp[6]->r = 60;
}

static void	init_clndr(t_rtv *rtv)
{
	create_vec(&(rtv->clndr[0]->crd), 0, 0, -600);
	create_vec(&(rtv->clndr[0]->dir), 1, 2, 0);
	rtv->clndr[0]->r = 40;
}

static void	init_cone(t_rtv *rtv)
{
	create_vec(&(rtv->cone[0]->crd), 0, 0, -570);
	create_vec(&(rtv->cone[0]->dir), 2, 2, 3);
	rtv->cone[0]->k = tan(10 * M_PI / 180);
}

static void	init_ftr(t_rtv *rtv)
{
	create_vec(&(rtv->ftr[0]->crd), 0, -400, -1500);
	create_vec(&(rtv->ftr[0]->dir), 0, 400, 10);
}

void		scene_8(t_rtv *rtv)
{
	rtv->t = 0;
	rtv->angle = 0;
	rtv->fong = 0;
	rtv->c_sp = 7;
	rtv->c_clndr = 1;
	rtv->c_cone = 1;
	rtv->c_ftr = 1;
	rtv->sp = create_sphere(7);
	rtv->clndr = create_cilinder(1);
	rtv->cone = create_cone(1);
	rtv->ftr = create_ftr(1);
	init_sphere(rtv);
	init_clndr(rtv);
	init_cone(rtv);
	init_ftr(rtv);
	create_vec(&(rtv->org), 0, 0, 1200);
	create_vec(&(rtv->dir), 0, 0, -1);
	create_vec(&(rtv->lgh), 0, 0, 150);
}
