/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 16:24:06 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:30:23 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

int		ft_exit(void *t)
{
	if (!t)
		exit(0);
	return (0);
}

int		ft_key(int key)
{
	(key == 53) ? exit(0) : 0;
	return (0);
}

void	start_raytacing(t_rtv *rtv)
{
	int		x;
	int		y;

	y = -1;
	while (++y < W)
	{
		x = -1;
		while (++x < H)
		{
			rtv->dir->x = (2 * ((x + 0.5) / W) - 1) * tan(30 * M_PI / 180);
			rtv->dir->y = (1 - 2 * ((y + 0.5) / H)) * tan(30 * M_PI / 180);
			rtv->angle = 1;
			rtv->fong = 0;
			rtv->clr = get_color(rtv, 100, 100, 100);
			intersection(rtv);
			(rtv->flag == 1) ? rtv->clr = get_color(rtv, 255, 50, 50) : 0;
			(rtv->flag == 2) ? rtv->clr = get_color(rtv, 86, 199, 129) : 0;
			(rtv->flag == 3) ? rtv->clr = get_color(rtv, 100, 124, 240) : 0;
			(rtv->flag == 4) ? rtv->clr = get_color(rtv, 191, 191, 191) : 0;
			(rtv->flag == 5) ? rtv->clr = get_color(rtv, 0, 0, 0) : 0;
			fill_image(x, y, rtv->clr, rtv->mlx);
		}
	}
}

int		main(int ac, char **av)
{
	t_rtv	*rtv;

	(void)av;
	if (ac < 2)
		errors(1);
	else if (ac != 2)
		errors(2);
	rtv = (t_rtv *)malloc(sizeof(t_rtv));
	check_scene(rtv, av[1]);
	rtv->mlx = (t_mlx *)malloc(sizeof(t_mlx));
	rtv->mlx->init = mlx_init();
	rtv->mlx->win = mlx_new_window(rtv->mlx->init, W, H, "RTv1");
	rtv->mlx->img = mlx_new_image(rtv->mlx->init, W, H);
	start_raytacing(rtv);
	mlx_hook(rtv->mlx->win, 17, 0L, ft_exit, NULL);
	mlx_hook(rtv->mlx->win, 2, 1, ft_key, NULL);
	mlx_put_image_to_window(rtv->mlx->init, rtv->mlx->win, rtv->mlx->img, 0, 0);
	mlx_loop(rtv->mlx->init);
	return (0);
}
