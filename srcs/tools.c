/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 08:28:53 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:29:22 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

t_vec	normalization(t_vec *p)
{
	t_vec	a;
	double	m;

	m = sqrt(multi_scalar(p, p));
	a.x = p->x / m;
	a.y = p->y / m;
	a.z = p->z / m;
	return (a);
}

void	create_vec(t_vec **v, double x, double y, double z)
{
	(*v) = (t_vec *)malloc(sizeof(t_vec));
	(*v)->x = x;
	(*v)->y = y;
	(*v)->z = z;
}

void	errors(int key)
{
	(key == 1) ? ft_putstr("Usage: ./RTv1 scene_1\n") : 0;
	(key == 2) ? ft_putstr("Only one agrument\n") : 0;
	(key == 3) ? ft_putstr("You must set arg from this list\n") : 0;
	exit(0);
}

void	ft_show_all_scenes(int key)
{
	ft_putstr("scene_1: sphere + plane\n");
	ft_putstr("scene_2: cilinder + plane\n");
	ft_putstr("scene_3: cone + plane\n");
	ft_putstr("scene_4: two spheres + plane\n");
	ft_putstr("scene_5: two cilinders + plane\n");
	ft_putstr("scene_6: two cones + plane\n");
	ft_putstr("scene_7: sphere + cilinder + cone + plane\n");
	ft_putstr("scene_8: sphere * 6 + cilinder + cone + plane\n");
	(key == 1) ? errors(3) : exit(0);
}

void	check_scene(t_rtv *rtv, char *av)
{
	if (ft_strcmp(av, "scene_1") == 0)
		scene_1(rtv);
	else if (ft_strcmp(av, "scene_2") == 0)
		scene_2(rtv);
	else if (ft_strcmp(av, "scene_3") == 0)
		scene_3(rtv);
	else if (ft_strcmp(av, "scene_4") == 0)
		scene_4(rtv);
	else if (ft_strcmp(av, "scene_5") == 0)
		scene_5(rtv);
	else if (ft_strcmp(av, "scene_6") == 0)
		scene_6(rtv);
	else if (ft_strcmp(av, "scene_7") == 0)
		scene_7(rtv);
	else if (ft_strcmp(av, "scene_8") == 0)
		scene_8(rtv);
	else if (ft_strcmp(av, "list") == 0)
		ft_show_all_scenes(0);
	else
		errors(1);
}
