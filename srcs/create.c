/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 06:48:20 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:03:20 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

t_sph	**create_sphere(int i)
{
	int		k;
	t_sph	**a;

	if (i <= 0)
	{
		ft_putstr("Problem with malloc: Sphere\n");
		exit(0);
	}
	a = (t_sph **)malloc(sizeof(t_sph *) * i);
	k = -1;
	while (++k < i)
		a[k] = (t_sph *)malloc(sizeof(t_sph));
	return (a);
}

t_clndr	**create_cilinder(int i)
{
	int		k;
	t_clndr	**a;

	if (i <= 0)
	{
		ft_putstr("Problem with malloc: Cilinder\n");
		exit(0);
	}
	a = (t_clndr **)malloc(sizeof(t_clndr *) * i);
	k = -1;
	while (++k < i)
		a[k] = (t_clndr *)malloc(sizeof(t_clndr));
	return (a);
}

t_cone	**create_cone(int i)
{
	int		k;
	t_cone	**a;

	if (i <= 0)
	{
		ft_putstr("Problem with malloc: Cone\n");
		exit(0);
	}
	a = (t_cone **)malloc(sizeof(t_cone *) * i);
	k = -1;
	while (++k < i)
		a[k] = (t_cone *)malloc(sizeof(t_cone));
	return (a);
}

t_ftr	**create_ftr(int i)
{
	int		k;
	t_ftr	**a;

	if (i <= 0)
	{
		ft_putstr("Problem with malloc: Footer\n");
		exit(0);
	}
	a = (t_ftr **)malloc(sizeof(t_ftr *) * i);
	k = -1;
	while (++k < i)
		a[k] = (t_ftr *)malloc(sizeof(t_ftr));
	return (a);
}
