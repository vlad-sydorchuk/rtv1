/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 08:03:39 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:42:15 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void	set_flag(t_rtv *rtv, int fl)
{
	if (rtv->t_m > 1e-4)
	{
		if (rtv->t_m > rtv->t)
		{
			rtv->t_m = rtv->t;
			rtv->flag = fl;
		}
	}
	else
	{
		rtv->t_m = rtv->t;
		rtv->flag = fl;
	}
}

void	sphera_ok(t_rtv *rtv, int i)
{
	t_vec	n;
	t_vec	n_l;
	double	t_angle;

	n = vec_minus(&rtv->p, rtv->sp[i]->crd);
	n = normalization(&n);
	n_l = vec_minus(rtv->lgh, &rtv->p);
	n_l = normalization(&n_l);
	t_angle = multi_scalar(&n_l, &n);
	if (t_angle < 0)
		t_angle = 0;
	if (t_angle > 1)
		t_angle = 1;
	if (t_angle > rtv->angle)
		rtv->angle = t_angle;
	phong(rtv, n, n_l);
}

void	clndr_ok(t_rtv *rtv, int i)
{
	t_vec	m_t;
	t_vec	d_n;
	t_vec	n;
	t_vec	n_l;
	double	t_angle;

	d_n = normalization(rtv->clndr[i]->dir);
	m_t = vec_multi_t(&d_n, rtv->t_m);
	t_angle = multi_scalar(rtv->dir, &m_t);
	m_t = vec_minus(rtv->org, rtv->clndr[i]->crd);
	t_angle = t_angle + multi_scalar(&m_t, &d_n);
	m_t = vec_multi_t(&d_n, t_angle);
	n = vec_minus(&rtv->p, rtv->clndr[i]->crd);
	n = vec_minus(&n, &m_t);
	n = normalization(&n);
	n_l = vec_minus(rtv->lgh, &rtv->p);
	n_l = normalization(&n_l);
	t_angle = multi_scalar(&n_l, &n);
	(t_angle < 0) ? t_angle = 0 : 0;
	if (t_angle > 1)
		t_angle = 1;
	if (t_angle > rtv->angle)
		rtv->angle = t_angle;
	phong(rtv, n, n_l);
}

void	cone_ok(t_rtv *rtv, int i)
{
	t_vec	m_t;
	t_vec	d_n;
	t_vec	n;
	t_vec	n_l;
	double	t_angle;

	d_n = normalization(rtv->cone[i]->dir);
	m_t = vec_multi_t(&d_n, rtv->t_m);
	t_angle = multi_scalar(rtv->dir, &m_t);
	m_t = vec_minus(rtv->org, rtv->cone[i]->crd);
	t_angle = t_angle + multi_scalar(&m_t, &d_n);
	t_angle = (1 + pow(rtv->cone[i]->k, 2)) * t_angle;
	m_t = vec_minus(&rtv->p, rtv->cone[i]->crd);
	n = vec_multi_t(&d_n, t_angle);
	n = vec_minus(&m_t, &n);
	n = normalization(&n);
	n_l = vec_minus(rtv->lgh, &rtv->p);
	n_l = normalization(&n_l);
	t_angle = multi_scalar(&n_l, &n);
	(t_angle < 0) ? t_angle = 0 : 0;
	(t_angle > 1) ? t_angle = 1 : 0;
	(t_angle > rtv->angle) ? rtv->angle = t_angle : 0;
	phong(rtv, n, n_l);
}

void	ftr_ok(t_rtv *rtv, int i)
{
	t_vec	n;
	t_vec	n_l;
	double	t_angle;

	n_l = vec_minus(rtv->lgh, &rtv->p);
	n_l = normalization(&n_l);
	n = normalization(rtv->ftr[i]->dir);
	t_angle = multi_scalar(&n_l, &n);
	if (t_angle < 0)
		t_angle = 0;
	if (t_angle > 1)
		t_angle = 1;
	if (t_angle > rtv->angle)
		rtv->angle = t_angle;
	phong(rtv, n, n_l);
}
