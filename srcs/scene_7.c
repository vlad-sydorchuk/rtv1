/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene_7.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 06:05:56 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:38:19 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

static void	init_sphere(t_rtv *rtv)
{
	create_vec(&(rtv->sp[0]->crd), 300, 0, -550);
	rtv->sp[0]->r = 160;
	create_vec(&(rtv->sp[1]->crd), -300, 0, -590);
	rtv->sp[1]->r = 160;
}

static void	init_clndr(t_rtv *rtv)
{
	create_vec(&(rtv->clndr[0]->crd), 0, 0, -600);
	create_vec(&(rtv->clndr[0]->dir), 1, 2, 0);
	rtv->clndr[0]->r = 70;
}

static void	init_cone(t_rtv *rtv)
{
	create_vec(&(rtv->cone[0]->crd), 0, 0, -570);
	create_vec(&(rtv->cone[0]->dir), 6, 2, 2);
	rtv->cone[0]->k = tan(10 * M_PI / 180);
}

static void	init_ftr(t_rtv *rtv)
{
	create_vec(&(rtv->ftr[0]->crd), 0, 0, -1600);
	create_vec(&(rtv->ftr[0]->dir), 0, 0, 1);
}

void		scene_7(t_rtv *rtv)
{
	rtv->t = 0;
	rtv->angle = 0;
	rtv->fong = 0;
	rtv->c_sp = 2;
	rtv->c_clndr = 1;
	rtv->c_cone = 1;
	rtv->c_ftr = 1;
	rtv->sp = create_sphere(2);
	rtv->clndr = create_cilinder(1);
	rtv->cone = create_cone(1);
	rtv->ftr = create_ftr(1);
	init_sphere(rtv);
	init_clndr(rtv);
	init_cone(rtv);
	init_ftr(rtv);
	create_vec(&(rtv->org), 0, 0, 1200);
	create_vec(&(rtv->dir), 0, 0, -1);
	create_vec(&(rtv->lgh), 0, 0, 150);
}
