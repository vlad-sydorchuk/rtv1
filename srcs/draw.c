/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 13:38:19 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:43:15 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void	fill_image(int x, int y, int rgb, t_mlx *mlx)
{
	int				bitspp;
	int				slen;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(mlx->img, &bitspp, &slen, &en);
	tmp = (mlx_get_color_value(mlx->init, rgb));
	if (y > 0 && y < H && x > 0 && x < W)
		ft_memcpy((void *)(image + slen * y + x *
			sizeof(int)), (void *)&tmp, 4);
}

void	phong(t_rtv *rtv, t_vec n, t_vec l)
{
	t_vec	v;
	t_vec	h;
	double	f;

	v = vec_plus(rtv->org, &rtv->p);
	v = normalization(&v);
	h = vec_plus(&v, &l);
	h = normalization(&h);
	f = multi_scalar(&n, &h);
	if (f < 0)
		f = 0;
	if (f > 1)
		f = 1;
	rtv->fong = f;
}

int		get_color(t_rtv *rtv, int r, int g, int b)
{
	rtv->fong = pow(rtv->fong, 500);
	r = (r * (rtv->angle + rtv->fong));
	if (r < 0)
		r = 0;
	else if (r > 255)
		r = 255;
	g = (g * (rtv->angle + rtv->fong));
	if (g < 0)
		g = 0;
	else if (g > 255)
		g = 255;
	b = (b * (rtv->angle + rtv->fong));
	if (b < 0)
		b = 0;
	else if (b > 255)
		b = 255;
	return (r << 16 | g << 8 | b);
}
