/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersection.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 23:58:39 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:26:34 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

static void	help1(t_rtv *rtv)
{
	int		i;

	if (rtv->flag != 1)
	{
		i = -1;
		while (++i < rtv->c_sp)
			if (intersection_sphere(rtv, &rtv->p, &rtv->tmp, i))
				if (rtv->t > 0 && rtv->t < rtv->dist)
					rtv->flag = 5;
	}
	if (rtv->flag != 2)
	{
		i = -1;
		while (++i < rtv->c_clndr)
			if (intersection_cilinder(rtv, &rtv->p, &rtv->tmp, i))
				if (rtv->t > 0 && rtv->t < rtv->dist)
					rtv->flag = 5;
	}
}

static void	help2(t_rtv *rtv)
{
	int		i;

	if (rtv->flag != 3)
	{
		i = -1;
		while (++i < rtv->c_cone)
			if (intersection_cone(rtv, &rtv->p, &rtv->tmp, i))
				if (rtv->t > 0 && rtv->t < rtv->dist)
					rtv->flag = 5;
	}
	if (rtv->flag != 4)
	{
		i = -1;
		while (++i < rtv->c_ftr)
			if (intersection_ftr(rtv, &rtv->p, &rtv->tmp, i))
				if (rtv->t > 0 && rtv->t < rtv->dist)
					rtv->flag = 5;
	}
}

static void	shadow(t_rtv *rtv)
{
	int		i;

	i = -1;
	while (++i < rtv->c_sp)
		rtv->flag == 1 ? sphera_ok(rtv, i) : 0;
	i = -1;
	while (++i < rtv->c_clndr)
		rtv->flag == 2 ? clndr_ok(rtv, i) : 0;
	i = -1;
	while (++i < rtv->c_cone)
		rtv->flag == 3 ? cone_ok(rtv, i) : 0;
	i = -1;
	while (++i < rtv->c_ftr)
		rtv->flag == 4 ? ftr_ok(rtv, i) : 0;
}

void		intersection(t_rtv *rtv)
{
	rtv->t = 0;
	rtv->t_m = 0;
	rtv->angle = 0;
	rtv->flag = 0;
	detect(rtv);
	if (rtv->flag)
	{
		rtv->p = vec_multi_t(rtv->dir, rtv->t_m);
		rtv->p = vec_plus(rtv->org, &rtv->p);
		rtv->tmp = vec_minus(rtv->lgh, &rtv->p);
		rtv->dist = sqrt(multi_scalar(&rtv->tmp, &rtv->tmp));
		rtv->tmp = normalization(&rtv->tmp);
		help1(rtv);
		help2(rtv);
		shadow(rtv);
	}
}
