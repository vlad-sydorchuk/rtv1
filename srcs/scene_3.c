/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene_3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 05:00:05 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 08:34:40 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

static void	init_ftr(t_rtv *rtv)
{
	create_vec(&(rtv->ftr[0]->crd), 0, -400, -600);
	create_vec(&(rtv->ftr[0]->dir), -45, 400, -70);
}

static void	init_cone(t_rtv *rtv)
{
	create_vec(&(rtv->cone[0]->crd), 0, 0, -570);
	create_vec(&(rtv->cone[0]->dir), 3, 2, 2);
	rtv->cone[0]->k = tan(10 * M_PI / 180);
}

void		scene_3(t_rtv *rtv)
{
	rtv->t = 0;
	rtv->angle = 0;
	rtv->fong = 0;
	rtv->c_sp = 0;
	rtv->c_clndr = 0;
	rtv->c_cone = 1;
	rtv->c_ftr = 1;
	rtv->cone = create_cone(1);
	rtv->ftr = create_ftr(1);
	init_cone(rtv);
	init_ftr(rtv);
	create_vec(&(rtv->org), 0, 0, 1000);
	create_vec(&(rtv->dir), 0, 0, -1);
	create_vec(&(rtv->lgh), 0, 0, 0);
}
