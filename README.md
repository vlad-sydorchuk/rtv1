# Ray-Tracer #

Summary: This mini-project is the first step to create a Raytracing program, for you to finally be able to render computer-generated images.

Tasks: https://cdn.intra.42.fr/pdf/pdf/961/rtv1.en.pdf

### How it work? ###

* Run Makefile.
* ./RTv1 [name_scene] (Example: ./RTv1 scene_1).
* ./RTv1 list - show all avaliable scenes

### You can see following fractols ###

* **Sphere**
![Sphere](img/img1.png)

* **Cilinder**
![Cilinder](img/img2.png)

* **Scene 7 (./RTv1 scene_7)**
![Scene_7](img/img3.png)

* **Scene 8 (./RTv1 scene_8)**
![Scene_8](img/img4.png)

