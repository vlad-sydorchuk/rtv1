/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 13:06:45 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 17:57:47 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include <wchar.h>
# include "../libft/libft.h"

size_t				g_c;

typedef enum		e_del
{
	ONE,
	TWO,
	BOTH
}					t_del;

typedef enum		e_len
{
	DEF,
	HH,
	H,
	L,
	LL,
	J,
	Z
}					t_len;

typedef struct		s_spec
{
	t_len			len;
	int				width;
	int				prec;
	char			hash;
	char			mn;
	char			ps;
	char			type;

}					t_spec;

/*
**					ft_printf.c
*/
size_t				ft_checking(const char *format, size_t i, t_spec *spec,
					va_list a);
int					start_printf(const char *format, va_list va_l);
int					ft_printf(const char *restrict format, ...);

/*
**					ft_init.c
*/
t_spec				*ft_init(void);

/*
**					ft_is.c
*/
int					is_flag(const char c);
int					is_len(const char c1, const char c2);
t_len				get_len(const char c1, const char c2);

/*
**					ft_additional.c
*/
void				ft_write(int f, const void *buf, size_t nb);
void				ft_putchar_v(const char c);
void				ft_putstr_v(const char *s);
char				*ft_lower(char *s);
char				*ft_utoa_base(unsigned long long value, int base);

/*
**					ft_additional2.c
*/
char				*add_precision(t_spec *spec, char *s);
char				*add_flags(t_spec *spec, char *s);
char				*add_min_width(t_spec *spec, char *s);

/*
**					ft_find.c
*/
size_t				find_flags(const char *str, size_t i, t_spec *sp);
size_t				find_width(const char *str, size_t i, t_spec *sp,
						va_list va_l);
size_t				find_prec(const char *str, size_t i, t_spec *sp,
						va_list va_l);
size_t				find_len(const char *str, size_t i, t_spec *sp);
size_t				find_spec(const char *str, size_t i, t_spec *sp);

/*
**					ft_get.c
*/
long long			get_arg(t_spec *spec, va_list va_l);
unsigned long long	get_unsigned_arg(t_spec *spec, va_list va_l);
char				*ft_join(char *s1, char *s2, t_del string);

/*
**					ft_number.c
*/
char				*to_int(t_spec *spec, va_list va_l);
char				*to_uint(t_spec *spec, va_list va_l);
char				*to_octal(t_spec *spec, va_list va_l);
char				*to_hex(t_spec *spec, va_list va_l);
char				*to_binary(t_spec *spec, va_list va_l);

/*
**					ft_pointer.c
*/
char				*ft_to_pointer(t_spec *spec, va_list va_l);

/*
**					ft_str.c
*/
char				*c_wstrtostr(wchar_t *s, int max_len);
char				*s_precision(char *s, t_spec *spec);
char				*s_width(char *s, t_spec *spec);

/*
**					ft_char.c
*/
void				char_width(char c, t_spec *spec);

/*
**					ft_to.c
*/
void				to_number(t_spec *spec, va_list va_l);
void				to_pointer(t_spec *spec, va_list va_l);
void				to_string(t_spec *spec, va_list va_l);
void				to_char(t_spec *spec, va_list va_l);
int					print_to(t_spec *spec, va_list va_l);

#endif
