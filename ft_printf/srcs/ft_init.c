/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 11:44:51 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/10 11:39:06 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

t_spec		*ft_init(void)
{
	t_spec	*spec;

	spec = (t_spec *)malloc(sizeof(t_spec));
	spec->len = DEF;
	spec->width = 0;
	spec->prec = -1;
	spec->hash = '\0';
	spec->mn = '\0';
	spec->ps = '\0';
	spec->type = '\0';
	return (spec);
}
