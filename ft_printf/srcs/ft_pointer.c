/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pointer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 09:55:05 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/10 13:21:42 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char		*ft_to_pointer(t_spec *spec, va_list va_l)
{
	char				*str;
	char				*end;
	unsigned long long	unsigned_nbr;

	unsigned_nbr = (unsigned long long)va_arg(va_l, void *);
	str = ft_utoa_base(unsigned_nbr, 16);
	str = ft_lower(str);
	end = ft_strjoin("0x", str);
	free(str);
	if (unsigned_nbr == 0 && spec->prec == 0)
	{
		free(end);
		end = ft_strdup("0x");
	}
	end = add_precision(spec, end);
	end = add_min_width(spec, end);
	return (end);
}
