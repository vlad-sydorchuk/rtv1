/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 11:55:17 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 15:32:49 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

long long				get_arg(t_spec *spec, va_list va_l)
{
	long long int	n;

	(spec->len == HH) ? n = (signed char)va_arg(va_l, int) : 0;
	(spec->len == H) ? n = (short)va_arg(va_l, int) : 0;
	(spec->len == L) ? n = (long)va_arg(va_l, long int) : 0;
	(spec->len == LL) ? n = (long long)va_arg(va_l, long long int) : 0;
	(spec->len == J) ? n = (intmax_t)va_arg(va_l, intmax_t) : 0;
	(spec->len == Z) ? n = (ssize_t)va_arg(va_l, ssize_t) : 0;
	(spec->len != HH &&
		spec->len != H &&
		spec->len != LL &&
		spec->len != L &&
		spec->len != J &&
		spec->len != Z) ? n = (int)va_arg(va_l, int) : 0;
	return (n);
}

unsigned long long		get_unsigned_arg(t_spec *spec, va_list va_l)
{
	unsigned long long int	n;

	(spec->len == HH) ? n = (unsigned char)va_arg(va_l, unsigned int) : 0;
	(spec->len == H) ? n = (unsigned short)va_arg(va_l, unsigned int) : 0;
	(spec->len == L)
	? n = (unsigned long)va_arg(va_l, unsigned long int) : 0;
	(spec->len == LL)
	? n = (unsigned long long)va_arg(va_l, unsigned long long int) : 0;
	(spec->len == J) ? n = (uintmax_t)va_arg(va_l, uintmax_t) : 0;
	(spec->len == Z) ? n = (size_t)va_arg(va_l, size_t) : 0;
	(spec->len != HH &&
		spec->len != H &&
		spec->len != LL &&
		spec->len != L &&
		spec->len != J &&
		spec->len != Z) ? n = (unsigned int)va_arg(va_l, unsigned int) : 0;
	return (n);
}

char					*ft_join(char *s1, char *s2, t_del string)
{
	char	*f;
	size_t	i;
	size_t	j;
	size_t	len;

	len = ft_strlen(s1) + ft_strlen(s2);
	if (!s1 || !s2)
		return (0);
	if (!(f = (char *)malloc(sizeof(char) * (len + 1))))
		return (0);
	i = -1;
	while (s1[++i])
		f[i] = s1[i];
	j = -1;
	while (s2[++j])
		f[i++] = s2[j];
	f[i] = '\0';
	(string == BOTH || string == ONE) ? free(s1) : 0;
	(string == BOTH || string == TWO) ? free(s2) : 0;
	return (f);
}
