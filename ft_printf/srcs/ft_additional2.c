/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_additional2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 14:35:52 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 14:27:00 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static char		*check_start_str(t_spec *spec, char *s, char *tmp)
{
	char	*result;

	if (s[0] == '-')
		result = ft_join(ft_strjoin("-", tmp), s + 1, ONE);
	else if (spec->type == 'o' && spec->hash == '#')
		result = ft_strjoin(tmp, s + 1);
	else if (s[1] == 'x')
		result = ft_join(ft_strjoin("0x", tmp), s + 2, ONE);
	else if (s[1] == 'X')
		result = ft_join(ft_strjoin("0X", tmp), s + 2, ONE);
	else if (s[1] == 'b')
		result = ft_join(ft_strjoin("0b", tmp), s + 2, ONE);
	else
		result = ft_strjoin(tmp, s);
	return (result);
}

char			*add_precision(t_spec *spec, char *s)
{
	char	*tmp;
	char	*result;
	size_t	len;
	size_t	size;
	size_t	i;

	len = ft_strlen(s);
	if (s[0] == '-' || (spec->type == 'o' && spec->hash == '#'))
		len = ft_strlen(s) - 1;
	else if (s[1] == 'x' || s[1] == 'X' || s[1] == 'b')
		len = ft_strlen(s) - 2;
	if (spec->prec > (int)len)
	{
		i = -1;
		size = (size_t)spec->prec - len;
		tmp = ft_strnew(size + 1);
		while (++i < size)
			tmp[i] = '0';
		tmp[i] = '\0';
		result = check_start_str(spec, s, tmp);
		free(tmp);
		free(s);
		return (result);
	}
	return (s);
}

char			*add_flags(t_spec *spec, char *s)
{
	char	*result;

	if (spec->ps == '+' && !ft_strchr(s, '-'))
		result = ft_strjoin("+", s);
	else if (spec->ps == ' ' && !ft_strchr(s, '-'))
		result = ft_strjoin(" ", s);
	else
		result = s;
	if ((spec->ps == '+' && !ft_strchr(s, '-')) ||
		(spec->ps == ' ' && !ft_strchr(s, '-')))
		free(s);
	return (result);
}

static char		*check_flag_null(char *s, char *tmp)
{
	char	*result;

	if (s[0] == ' ')
		result = ft_join(ft_strjoin(" ", tmp), s + 1, ONE);
	else if (s[0] == '-')
		result = ft_join(ft_strjoin("-", tmp), s + 1, ONE);
	else if (s[0] == '+')
		result = ft_join(ft_strjoin("+", tmp), s + 1, ONE);
	else if (s[1] == 'x')
		result = ft_join(ft_strjoin("0x", tmp), s + 2, ONE);
	else if (s[1] == 'X')
		result = ft_join(ft_strjoin("0X", tmp), s + 2, ONE);
	else if (s[1] == 'b')
		result = ft_join(ft_strjoin("0b", tmp), s + 2, ONE);
	else
		result = ft_strjoin(tmp, s);
	return (result);
}

char			*add_min_width(t_spec *spec, char *s)
{
	char	*tmp;
	char	*result;

	if ((spec->width > 0) && (spec->width > (int)ft_strlen(s)))
	{
		spec->width = spec->width - (int)ft_strlen(s);
		tmp = ft_strnew((size_t)spec->width);
		(spec->mn == '0' && spec->prec < 0)
		? (tmp = ft_memset(tmp, '0', (size_t)spec->width))
		: (tmp = ft_memset(tmp, ' ', (size_t)spec->width));
		if (spec->mn == '0')
			result = check_flag_null(s, tmp);
		else if (spec->mn == '-')
			result = ft_strjoin(s, tmp);
		else
			result = ft_strjoin(tmp, s);
		free(tmp);
		free(s);
		return (result);
	}
	return (s);
}
