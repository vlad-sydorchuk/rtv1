/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_to.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 13:25:15 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 16:39:55 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void		to_number(t_spec *spec, va_list va_l)
{
	char	*str;

	if (spec->type == 'd' || spec->type == 'i')
		str = to_int(spec, va_l);
	else if (spec->type == 'u')
		str = to_uint(spec, va_l);
	else if (spec->type == 'o')
		str = to_octal(spec, va_l);
	else if (spec->type == 'x' || spec->type == 'X')
		str = to_hex(spec, va_l);
	else if (spec->type == 'b')
		str = to_binary(spec, va_l);
	else
		str = ft_strdup("");
	ft_putstr_v(str);
	free(str);
}

void		to_pointer(t_spec *spec, va_list va_l)
{
	char	*str;

	str = ft_to_pointer(spec, va_l);
	ft_putstr_v(str);
	free(str);
}

void		to_string(t_spec *spec, va_list va_l)
{
	char	*str;

	if (spec->len == L)
		str = c_wstrtostr(va_arg(va_l, wchar_t *), spec->prec);
	else
	{
		str = va_arg(va_l, char *);
		if (!str)
			str = ft_strdup("(null)");
		else
			str = ft_strdup(str);
	}
	str = s_precision(str, spec);
	str = s_width(str, spec);
	ft_putstr_v(str);
	free(str);
}

void		to_char(t_spec *spec, va_list va_l)
{
	char	c;

	if (spec->type == 'c' && spec->len == L)
		c = (char)va_arg(va_l, wint_t);
	else if (spec->type == 'c')
		c = va_arg(va_l, int);
	else
		c = spec->type;
	char_width(c, spec);
}

int			print_to(t_spec *spec, va_list va_l)
{
	if (ft_strchr("bdiouxX", spec->type))
		to_number(spec, va_l);
	else if (spec->type == 'p')
		to_pointer(spec, va_l);
	else if (spec->type == 's')
		to_string(spec, va_l);
	else
		to_char(spec, va_l);
	return (1);
}
