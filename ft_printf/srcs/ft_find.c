/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 12:19:34 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 14:44:48 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

size_t		find_flags(const char *str, size_t i, t_spec *sp)
{
	while (is_flag(str[i]))
	{
		if (str[i] == '#')
			sp->hash = '#';
		else if (str[i] == '-' || str[i] == '0')
		{
			if (str[i] == '0' && sp->mn != '-')
				sp->mn = str[i];
			else if (str[i] == '-')
				sp->mn = str[i];
		}
		else if (str[i] == '+' || str[i] == ' ')
		{
			if (str[i] == ' ' && sp->ps != '+')
				sp->ps = str[i];
			else if (str[i] == '+')
				sp->ps = str[i];
		}
		++i;
	}
	return (i);
}

size_t		find_width(const char *str, size_t i, t_spec *sp, va_list va_l)
{
	if (ft_isdigit(str[i]))
	{
		sp->width = ft_atoi(str + i);
		while (ft_isdigit(str[i]))
			++i;
	}
	else if (str[i] == '*')
	{
		sp->width = va_arg(va_l, int);
		if (sp->width < 0)
		{
			sp->width *= -1;
			sp->mn = '-';
		}
		++i;
	}
	return (i);
}

size_t		find_prec(const char *str, size_t i, t_spec *sp, va_list va_l)
{
	if (str[i] == '.')
	{
		++i;
		sp->prec = 0;
		if (ft_isdigit(str[i]))
		{
			sp->prec = ft_atoi(str + i);
			while (ft_isdigit(str[i]))
				++i;
		}
		else if (str[i] == '*')
		{
			sp->prec = va_arg(va_l, int);
			++i;
		}
	}
	return (i);
}

size_t		find_len(const char *str, size_t i, t_spec *sp)
{
	t_len	len;

	while (is_len(str[i], str[i + 1]))
	{
		len = get_len(str[i], str[i + 1]);
		(sp->len < len) ? sp->len = len : 0;
		i += is_len(str[i], str[i + 1]);
	}
	return (i);
}

size_t		find_spec(const char *str, size_t i, t_spec *sp)
{
	char c;

	c = str[i];
	if (c == 'D' || c == 'O' || c == 'U' || c == 'C' || c == 'S')
	{
		c = (char)ft_tolower(c);
		sp->len = L;
	}
	sp->type = c;
	return (i);
}
