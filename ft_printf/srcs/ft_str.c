/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 09:30:25 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 16:36:47 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static char		*gene_wint(wint_t wint, int len)
{
	char	*s;

	s = (char *)malloc(sizeof(char) * (len + 1));
	s[len--] = '\0';
	if (len == 0)
	{
		s[len] = (char)wint;
		return (s);
	}
	s[len--] = (char)((wint & 63) + 128);
	if (len == 0)
	{
		s[len] = (char)(((wint & 1984) >> 6) + 192);
		return (s);
	}
	s[len--] = (char)(((wint & 4032) >> 6) + 128);
	if (len == 0)
	{
		s[len] = (char)(((wint & 61440) >> 12) + 224);
		return (s);
	}
	s[len--] = (char)(((wint & 258048) >> 12) + 128);
	s[len] = (char)(((wint & 1835008) >> 18) + 240);
	return (s);
}

static char		*c_winttostr(wint_t wint)
{
	if (wint < 128)
		return (gene_wint(wint, 1));
	if (wint < 2048)
		return (gene_wint(wint, 2));
	if (wint < 65536)
		return (gene_wint(wint, 3));
	if (wint < 1114112)
		return (gene_wint(wint, 4));
	return (NULL);
}

char			*c_wstrtostr(wchar_t *s, int max_len)
{
	char	*wint_tmp;
	char	*tmp;
	char	*end;
	size_t	len;

	if (!s)
		return (ft_strdup("(null)"));
	len = -1;
	end = ft_strdup("");
	while (s[++len])
	{
		tmp = end;
		wint_tmp = c_winttostr(s[len]);
		end = ft_strjoin(end, wint_tmp);
		free(wint_tmp);
		if (max_len >= 0 && (int)ft_strlen(end) > max_len)
		{
			free(end);
			end = tmp;
			break ;
		}
		free(tmp);
	}
	return (end);
}

char			*s_precision(char *s, t_spec *spec)
{
	char	*res;
	int		i;

	if ((spec->prec >= 0) && (spec->prec < (int)ft_strlen(s)))
	{
		i = -1;
		res = ft_strnew((size_t)spec->prec);
		while (++i < spec->prec)
			res[i] = s[i];
		res[i] = '\0';
		return (res);
	}
	return (s);
}

char			*s_width(char *s, t_spec *spec)
{
	char	*tmp;
	int		i;

	if ((spec->width > 0) && (spec->width > (int)ft_strlen(s)))
	{
		spec->width = spec->width - (int)ft_strlen(s);
		tmp = ft_strnew((size_t)spec->width);
		i = -1;
		while (++i < spec->width)
		{
			if (spec->mn == '0')
				tmp[i] = '0';
			else
				tmp[i] = ' ';
		}
		tmp[i] = '\0';
		(spec->mn == '-')
		? (s = ft_join(s, tmp, ONE))
		: (s = ft_join(tmp, s, TWO));
		free(tmp);
	}
	return (s);
}
