/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_additional.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 13:11:49 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 17:57:45 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void		ft_write(int f, const void *buf, size_t nb)
{
	g_c += write(f, buf, nb);
}

void		ft_putchar_v(const char c)
{
	ft_write(1, &c, 1);
}

void		ft_putstr_v(const char *s)
{
	if (!s)
		return ;
	ft_write(1, s, ft_strlen(s));
}

char		*ft_lower(char *s)
{
	size_t	i;

	i = -1;
	while (s[++i])
		if (s[i] >= 'A' && s[i] <= 'Z')
			s[i] = s[i] + 32;
	return (s);
}

char		*ft_utoa_base(unsigned long long val, int base)
{
	int					i;
	unsigned long long	tmp;
	char				*res;
	char				*b;

	b = "0123456789ABCDEF";
	i = 1;
	tmp = val;
	while (val /= base)
		++i;
	if (!(res = (char *)malloc(sizeof(char) * (i + 1))))
		return (NULL);
	res[i] = '\0';
	while (--i >= 0)
	{
		res[i] = b[tmp % base];
		tmp /= base;
	}
	return (res);
}
