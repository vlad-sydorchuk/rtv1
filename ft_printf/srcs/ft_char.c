/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_char.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 13:08:27 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 17:31:26 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	char_width(char c, t_spec *spec)
{
	int		t;

	if (spec->width > 1)
	{
		t = spec->width;
		(spec->mn == '-') ? ft_putchar_v(c) : 0;
		while (--t > 0)
			(spec->mn == '0') ? ft_putchar_v('0') : ft_putchar_v(' ');
		(spec->mn != '-') ? ft_putchar_v(c) : 0;
	}
	else
		ft_putchar_v(c);
}
