/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 11:30:31 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 17:57:49 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

size_t		ft_checking(const char *format, size_t i, t_spec *spec, va_list a)
{
	size_t	tmp;

	tmp = 0;
	while (format[i] && i > tmp)
	{
		tmp = i;
		i = find_flags(format, i, spec);
		i = find_width(format, i, spec, a);
		i = find_prec(format, i, spec, a);
		i = find_len(format, i, spec);
	}
	if (format[i])
		i = find_spec(format, i, spec);
	return (i);
}

int			start_printf(const char *format, va_list va_l)
{
	size_t		i;
	t_spec		*spec;

	i = -1;
	while (format[++i])
	{
		if (format[i] == '%')
		{
			spec = ft_init();
			i = ft_checking(format, i + 1, spec, va_l);
			if (format[i] == '\0')
				break ;
			print_to(spec, va_l);
			free(spec);
		}
		else
			ft_write(1, format + i, 1);
	}
	return (0);
}

int			ft_printf(const char *restrict format, ...)
{
	int			count;
	va_list		va_l;

	if (!format)
		return (-1);
	va_start(va_l, format);
	start_printf(format, va_l);
	va_end(va_l);
	count = (int)g_c;
	g_c = 0;
	return (count);
}
