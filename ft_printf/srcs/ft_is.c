/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 12:10:46 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 15:33:27 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			is_flag(const char c)
{
	if (c == '+' || c == '-' || c == ' ' || c == '0' || c == '#')
		return (1);
	return (0);
}

int			is_len(const char c1, const char c2)
{
	if ((c1 == 'h' && c2 == 'h') || (c1 == 'l' && c2 == 'l'))
		return (2);
	if (c1 == 'h' || c1 == 'l' || c1 == 'j' || c1 == 'z')
		return (1);
	return (0);
}

t_len		get_len(const char c1, const char c2)
{
	if (c1 == 'h' && c2 == 'h')
		return (HH);
	if (c1 == 'l' && c2 == 'l')
		return (LL);
	if (c1 == 'h')
		return (H);
	if (c1 == 'l')
		return (L);
	if (c1 == 'j')
		return (J);
	if (c1 == 'z')
		return (Z);
	return (DEF);
}
