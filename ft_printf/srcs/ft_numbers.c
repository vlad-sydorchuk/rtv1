/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_numbers.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 13:17:11 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/11 16:38:04 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char		*to_int(t_spec *spec, va_list va_l)
{
	char				*s;
	char				*end;
	long long			nbr;
	int					minus;

	minus = 0;
	nbr = get_arg(spec, va_l);
	(nbr < 0 && (minus = 1)) ? nbr *= -1 : 0;
	s = ft_utoa_base((unsigned long long)nbr, 10);
	(minus == 1)
	? (end = ft_join("-", s, TWO))
	: (end = s);
	if (nbr == 0 && spec->prec == 0)
	{
		free(end);
		end = ft_strdup("");
	}
	end = add_precision(spec, end);
	end = add_flags(spec, end);
	end = add_min_width(spec, end);
	return (end);
}

char		*to_uint(t_spec *spec, va_list va_l)
{
	char				*end;
	unsigned long long	unsigned_nbr;

	unsigned_nbr = get_unsigned_arg(spec, va_l);
	end = ft_utoa_base(unsigned_nbr, 10);
	if (unsigned_nbr == 0 && spec->prec == 0)
	{
		free(end);
		end = ft_strdup("");
	}
	end = add_precision(spec, end);
	end = add_min_width(spec, end);
	return (end);
}

char		*to_octal(t_spec *spec, va_list va_l)
{
	char				*s;
	char				*end;
	unsigned long long	unsigned_nbr;

	unsigned_nbr = get_unsigned_arg(spec, va_l);
	s = ft_utoa_base(unsigned_nbr, 8);
	if (spec->hash == '#' && s[0] != '0')
	{
		end = ft_strjoin("0", s);
		free(s);
	}
	else
		end = s;
	if (unsigned_nbr == 0 && spec->prec == 0 && spec->hash != '#')
	{
		free(end);
		end = ft_strdup("");
	}
	end = add_precision(spec, end);
	end = add_min_width(spec, end);
	return (end);
}

char		*to_hex(t_spec *spec, va_list va_l)
{
	char				*s;
	char				*end;
	unsigned long long	unsigned_nbr;

	unsigned_nbr = get_unsigned_arg(spec, va_l);
	s = ft_utoa_base(unsigned_nbr, 16);
	(spec->type == 'x') ? s = ft_lower(s) : 0;
	if (spec->hash == '#' && s[0] != '0')
	{
		if (spec->type == 'x')
			end = ft_join("0x", s, TWO);
		else if (spec->type == 'X')
			end = ft_join("0X", s, TWO);
	}
	else
		end = s;
	if (unsigned_nbr == 0 && spec->prec == 0)
	{
		free(end);
		end = ft_strdup("");
	}
	end = add_precision(spec, end);
	end = add_min_width(spec, end);
	return (end);
}

char		*to_binary(t_spec *spec, va_list va_l)
{
	char				*s;
	char				*end;
	unsigned long long	unsigned_nbr;

	unsigned_nbr = get_unsigned_arg(spec, va_l);
	s = ft_utoa_base(unsigned_nbr, 2);
	(spec->type == 'b' && spec->hash == '#' && s[0] != '0')
	? (end = ft_join("0b", s, TWO))
	: (end = s);
	if (unsigned_nbr == 0 && spec->prec == 0)
	{
		free(end);
		end = ft_strdup("");
	}
	end = add_precision(spec, end);
	end = add_min_width(spec, end);
	return (end);
}
