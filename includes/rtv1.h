/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 16:27:14 by vsydorch          #+#    #+#             */
/*   Updated: 2017/10/29 09:02:55 by vsydorch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H
# include "../minilibX/mlx.h"
# include "../ft_printf/includes/ft_printf.h"
# include <math.h>
# define W 1000
# define H 1000

typedef struct	s_disc
{
	double	a;
	double	b;
	double	c;
	double	d;
}				t_disc;

typedef struct	s_vec
{
	double	x;
	double	y;
	double	z;
}				t_vec;

typedef	struct	s_sph
{
	t_vec	*crd;
	double	r;
	double	angle;
	int		clr;
}				t_sph;

typedef struct	s_clndr
{
	t_vec	*crd;
	t_vec	*dir;
	double	r;
}				t_clndr;

typedef struct	s_cone
{
	t_vec	*crd;
	t_vec	*dir;
	double	k;
}				t_cone;

typedef struct	s_ftr
{
	t_vec	*crd;
	t_vec	*dir;
}				t_ftr;

typedef struct	s_mlx
{
	void	*init;
	void	*win;
	void	*img;
}				t_mlx;

typedef	struct	s_ray
{
	t_vec	*crd;
	t_vec	*dir;
}				t_ray;

typedef	struct	s_rtv
{
	t_mlx	*mlx;
	t_sph	**sp;
	t_clndr	**clndr;
	t_cone	**cone;
	t_ftr	**ftr;
	t_vec	*org;
	t_vec	*dir;
	t_vec	*lgh;
	t_vec	p;
	t_vec	tmp;
	double	dist;
	double	t1;
	double	t2;
	double	t;
	double	t_m;
	double	fong;
	int		flag;
	int		clr;
	double	angle;
	int		c_sp;
	int		c_clndr;
	int		c_cone;
	int		c_ftr;
}				t_rtv;

/*
**				create.c
*/
t_sph			**create_sphere(int i);
t_clndr			**create_cilinder(int i);
t_cone			**create_cone(int i);
t_ftr			**create_ftr(int i);

/*
**				tools.c
*/
t_vec			normalization(t_vec *p);
void			create_vec(t_vec **v, double x, double y, double z);
void			errors(int key);
void			ft_show_all_scenes(int key);
void			check_scene(t_rtv *rtv, char *av);

/*
**				initialization.c
*/
void			init_zero(t_rtv *rtv);

/*
**				color.c
*/
void			set_flag(t_rtv *rtv, int fl);
void			sphera_ok(t_rtv *rtv, int i);
void			clndr_ok(t_rtv *rtv, int i);
void			cone_ok(t_rtv *rtv, int i);
void			ftr_ok(t_rtv *rtv, int i);

/*
**				scene
*/
void			scene_1(t_rtv *rtv);
void			scene_2(t_rtv *rtv);
void			scene_3(t_rtv *rtv);
void			scene_4(t_rtv *rtv);
void			scene_5(t_rtv *rtv);
void			scene_6(t_rtv *rtv);
void			scene_7(t_rtv *rtv);
void			scene_8(t_rtv *rtv);
void			scene_9(t_rtv *rtv);

void			create_vec(t_vec **v, double x, double y, double z);
void			show_vec(t_vec *a, char *s);
int				intersection_sphere(t_rtv *rtv, t_vec *org, t_vec *dir, int i);
int				intersection_cilinder(t_rtv *rtv, t_vec *org, t_vec *dir,
				int i);
int				intersection_cone(t_rtv *rtv, t_vec *org, t_vec *dir, int i);
int				intersection_ftr(t_rtv *rtv, t_vec *org, t_vec *dir, int i);
void			detect(t_rtv *rtv);
void			intersection(t_rtv *rtv);

/*
**				vector_operations.c
*/
double			multi_scalar(t_vec *d1, t_vec *d2);
t_vec			vec_minus(t_vec *d1, t_vec *d2);
t_vec			vec_plus(t_vec *d1, t_vec *d2);
t_vec			vec_multi_t(t_vec *d1, double t);
t_vec			vec_invert(t_vec *d1);

/*
**				draw.c
*/
void			fill_image(int x, int y, int rgb, t_mlx *mlx);
int				get_color(t_rtv *rtv, int r, int g, int b);
void			phong(t_rtv *rtv, t_vec n, t_vec l);

/*
**				intersection.c
*/
t_vec			normalization(t_vec *p);
#endif
