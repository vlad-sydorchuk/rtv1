# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vsydorch <vsydorch@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/18 17:33:37 by vsydorch          #+#    #+#              #
#    Updated: 2017/10/29 09:03:01 by vsydorch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 		= RTv1

LIB 		= ft_printf/libftprintf.a
SRCS 		=  	srcs/main.c 				\
				srcs/vector_operations.c 	\
				srcs/initialization.c 		\
				srcs/draw.c 				\
				srcs/intersection.c 		\
				srcs/intersection_figure.c 	\
				srcs/create.c 				\
				srcs/color.c 				\
				srcs/tools.c 				\
				srcs/scene_1.c 				\
				srcs/scene_2.c 				\
				srcs/scene_3.c 				\
				srcs/scene_4.c 				\
				srcs/scene_5.c 				\
				srcs/scene_6.c 				\
				srcs/scene_7.c 				\
				srcs/scene_8.c	

HEADERS		= includes/rtv1.h
FLAGS 		= -Wall -Wextra -Werror 
LOCAL 		= -I minilibX -g -L minilibX
FLAGS_MLX 	= -lmlx -framework OpenGl -framework AppKit
BINS 		= $(SRCS:.c=.o)
all: $(NAME)

libclean:
	make -C ft_printf/ clean
libfclean:
	make -C ft_printf/ fclean
$(NAME): $(BINS)
	make -C ft_printf/
	gcc -o $(NAME) $(BINS) $(FLAGS) $(LOCAL) $(FLAGS_MLX) $(LIB)
%.o: %.c
	gcc $(FLAGS) -c -o $@ $<
clean: libclean
	/bin/rm -f $(BINS)
fclean: libfclean clean
	/bin/rm -f $(NAME)
re: fclean all
